module.exports = {
    data1: {
        dtSpecialty: 'Urology',
        dtLocation: 'Malaysia',
        dtDate: new Date('31 August 2020')
    },
    data2: {
        dtSpecialty: 'Dermatology',
        dtLocation: 'Singapore',
        dtDate: new Date('31 August 2020')
    },
    data3: {
        dtSpecialty: 'Gynae(Oncology)',
        dtLocation: 'Hong Kong',
        dtDate: new Date('31 August 2020')
    }
}